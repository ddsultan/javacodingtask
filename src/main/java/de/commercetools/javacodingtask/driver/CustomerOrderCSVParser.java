package de.commercetools.javacodingtask.driver;

import de.commercetools.javacodingtask.models.Customer;
import de.commercetools.javacodingtask.models.Order;
import io.sphere.sdk.models.Base;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author ddsultan
 */
public class CustomerOrderCSVParser {

    private static String path;

    /**
     * Main method takes path reads the CSV file specified and return list Lists
     * of Customer List and one's corresponding Order orders list
     *
     *
     * @return two pair: List of Customer customers and corresponding List of
     * Orders
     */
    public static List getCustomersAndOrders() {
        List<Customer> customers = new ArrayList();
        List<Order> customerOrders = new ArrayList();
        List customersAndOrders = new ArrayList();

        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            Stream<String> lines = reader.lines();

            lines.distinct()//remove duplicate headers/lines if any
                    .skip(1)//skip the header
                    .filter((line) -> (!line.trim().equals("")))//filter empty lines if any
                    .collect(Collectors.toList())
                    .forEach((line) -> {

                        String[] lineSplit = line.split(",");

                        Base[] customerAndOrder = createCustomerAndOrder(lineSplit);

                        Customer customer = (Customer) customerAndOrder[0];
                        Order order = (Order) customerAndOrder[1];

                        customers.add(customer);
                        customerOrders.add(order);
                    });
        } catch (IOException ex) {
            System.err.format("IOException: %s%n", ex);
        }

        customersAndOrders.add(0, customers);
        customersAndOrders.add(1, customerOrders);
        return customersAndOrders;

    }

    /**
     * *
     * Return Customer and Order pair using the helper methods
     *
     * @param splittedLine String line parsed from the CSV file
     * @return Array of two Base class object (Customer and Order)
     */
    private static Base[] createCustomerAndOrder(String[] splittedLine) {
        int count = 0;
        String[] customerProps = new String[8];
        String[] orderProps = new String[2];

        for (String property : splittedLine) {

            //first eight  are Customer properties 
            if (count <= 7) {
                customerProps[count] = property;
            } else if (count > 7) {
                orderProps[count - customerProps.length] = property;
            }
            count++;
        }

        Customer customer = createCustomerObjectWithProperties(customerProps);
        Order order = createOrderObjectWithProperties(customer.getId(), orderProps);

        Base[] result = {customer, order};
        return result;
    }

    /**
     * *
     * Helper method. Returns an Customer object at a time with specified
     * properties
     *
     *
     *
     * @param customerProps Array of String Customer properties parsed from the
     * CSV line
     * @return Customer model object binding the properties with customerProps
     */
    private static Customer createCustomerObjectWithProperties(String[] customerProps) {
        Customer customer = new Customer();

        customer.setId(customerProps[0]);//id
        customer.setFirstName(customerProps[1]);//first
        customer.setLastName(customerProps[2]);//last
        customer.setAge(Integer.parseInt(customerProps[3]));//age
        customer.setStreet(customerProps[4]);//street
        customer.setCity(customerProps[5]);//city
        customer.setState(customerProps[6]);//state
        customer.setZip(customerProps[7]);//zip

        return customer;
    }

    /**
     * Helper method. Returns an Order object binding its customer id at a time
     * with specified properties
     *
     * @param customerId the ID of the customer where order belongs to
     * @param orderProps Array of String Order properties parsed from the CSV
     * line
     * @return Order model object binding the properties with orderProps
     */
    private static Order createOrderObjectWithProperties(String customerId, String[] orderProps) {
        Locale locale = Locale.US;

        Order order = new Order();

        order.setId(UUID.randomUUID().toString());
        order.setCustomerId(customerId);
        order.setCurrency(Currency.getInstance(locale));
        order.setPick(orderProps[1]);//pick

        try {
            order.setCentAmount(NumberFormat.getCurrencyInstance(locale).parse(orderProps[0]).longValue());//amount
        } catch (ParseException e) {
            e.getMessage();
        }
        return order;
    }

    /**
     * @return the path
     */
    public static String getPath() {
        return path;
    }

    /**
     * @param pathParam the path to set
     */
    public static void setPath(String pathParam) {
        path = pathParam;
    }
}
