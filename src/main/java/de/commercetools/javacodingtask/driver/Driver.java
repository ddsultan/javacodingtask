package de.commercetools.javacodingtask.driver;

import de.commercetools.javacodingtask.client.Client;
import de.commercetools.javacodingtask.client.ClientFactory;
import de.commercetools.javacodingtask.errors.ServiceUnavailableException;
import de.commercetools.javacodingtask.models.Customer;
import de.commercetools.javacodingtask.models.Order;
import java.util.List;

/**
 *
 * @author ddsultan
 */
public class Driver {

    /**
     * main Driver method to be called when application is run
     *
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("Lets GO!");

        //first we buffer up the file content 
        //read each line as Customer and Order entities 
        //push them into respective lists 
        //and call importer to the server through the client  
        String csvDir = System.getProperty("user.dir") + "/data/sample.csv";

        CustomerOrderCSVParser.setPath(csvDir);

        List<Customer> customers = (List<Customer>) CustomerOrderCSVParser.getCustomersAndOrders().get(0);
        List<Order> orders = (List<Order>) CustomerOrderCSVParser.getCustomersAndOrders().get(1);

        Client client = ClientFactory.create("ssenlighta23@gmail.com");

        uploadBatchesOfCustomers(client, customers);
        uploadBatchesOfOrders(client, orders);

//        System.out.printf("customers SIZE %s, order SIZE:%s", customers.size(), orders.size());
    }

    /**
     * Uploads the List of Customer customers to specified Client client
     *
     * @param client Client object responsible for importing
     * @param customers List of Customer customers to import
     */
    private static void uploadBatchesOfCustomers(Client client, List<Customer> customers) {

        try {
            client.importCustomer(customers);

        } catch (ServiceUnavailableException e) {
            System.out.println("THERE WAS A PROBLEM UPLOADING customer BATCHES" + e.getMessage());

        }
    }

    /**
     * Uploads the List of Order orders to specified Client client
     *
     * @param client Client object responsible for importing
     * @param orders List of Order orders to import
     */
    private static void uploadBatchesOfOrders(Client client, List<Order> orders) {

        try {
            client.importOrders(orders);

        } catch (ServiceUnavailableException e) {
            System.out.println("THERE WAS A PROBLEM UPLOADING order BATCHES" + e.getMessage());
        }
    }

}
