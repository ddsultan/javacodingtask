
import de.commercetools.javacodingtask.driver.CustomerOrderCSVParser;
import de.commercetools.javacodingtask.models.Customer;
import de.commercetools.javacodingtask.models.Order;
import java.util.List;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author ddsultan
 */
public class CustomerOrderCSVParserTest {

    /**
     * Test the number of Customer list of customer size against Order orders List size
     *
     * @throws Exception
     */
    @Test
    public void testCustomersANDOrders() throws Exception {
        String csvDir = System.getProperty("user.dir") + "/data/sample.csv";

        CustomerOrderCSVParser.setPath(csvDir);
        List<Customer> customers = (List<Customer>) CustomerOrderCSVParser.getCustomersAndOrders().get(0);
        List<Order> orders = (List<Order>) CustomerOrderCSVParser.getCustomersAndOrders().get(1);

        assertEquals("Mapping customers and orders working incorrectly!", customers.size(), orders.size());
    }

}
