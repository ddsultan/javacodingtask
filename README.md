# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
* I have not upload the original csv file here 
([https://s3-eu-west-1.amazonaws.com/commercetools-datastore/codingtasks/random-customer-order-testdata-1.csv](https://s3-eu-west-1.amazonaws.com/commercetools-datastore/codingtasks/random-customer-order-testdata-1.csv))
so as to save some time and traffic) and place it into data directory.
* The App will import data from a CSV file and upload it to a server as JSON (POJO 
* mapping).
* 0.1.0
* Technologies(Requirements): Java 8, Maven 3, JUnit
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Configuration: git clone the repo (access may be required)
* Using maven build the project and run it 
* Dependencies are in the pom file
* With a help of maven you can test as a whole or by file (IDE is good option)
* Deployment instructions: with Maven

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo ddsultan